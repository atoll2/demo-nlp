scalaVersion := "2.10.1"

resolvers += "Local Maven Repository" at "file:///home/atoll/.m2/repository"

libraryDependencies += "edu.arizona.sista" % "processors" % "3.2.0"

libraryDependencies += "edu.stanford.nlp" % "stanford-corenlp" % "3.2.0"
