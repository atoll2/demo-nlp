
import org.grammaticalframework.reader.{Concrete}
import org.grammaticalframework.{Linearizer, Parser, PGF, PGFBuilder}
import org.grammaticalframework.Trees.PrettyPrinter

import java.io.FileInputStream

object Hello extends App {

    val pgf = PGFBuilder.fromInputStream(new FileInputStream("Foods.pgf"))
    val parser = new Parser(pgf, "FoodsEng")
    val linearizer = new Linearizer(pgf, pgf.concrete("FoodsIta"))

    val txt2 = args
    val txt = "wine is delicious"
    val trees = parser.parse(txt2.split(" ")).getTrees
    var s = ""
    trees.foreach( t => {
      s += linearizer.linearizeString(t)
    })

   println(s)

}
