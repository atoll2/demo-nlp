import nltk

sentence = """Pierre Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29"""

tokens = nltk.word_tokenize(sentence)

tagged = nltk.pos_tag(tokens)

entities = nltk.chunk.ne_chunk(tagged)

print entities
